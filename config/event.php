<?php

/**
 * @desc 事件定义文件
 * @author Tinywan(ShaoBo Wan)
 * @date 2021/12/15 22:55
 */

return [
    // 事件监听
    'listener'    => [],

    // 事件订阅器
    'subscriber' => [],
];